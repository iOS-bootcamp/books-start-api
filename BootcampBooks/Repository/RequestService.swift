//
//  RequestService.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import RxSwift
import ObjectMapper

class RequestService {
    
    class func call<T: Mappable>(url: String, method: HTTPMethod = .get) -> Observable<T> {
        return Observable.create { observable in
            let request = Alamofire.request(url)
                .validate()
                .responseObject { (response: DataResponse<T>) in
                    
                    switch response.result {
                    case .success(let result):
                        print("List: \(result)")
                        observable.onNext(result)
                        observable.onCompleted()
                        
                    case .failure(let error):
                        print("ERROR: \(error)")
                        observable.onError(error)
                    }
                }
            return Disposables.create {
                request.cancel()
            }
            }
        }
}

public enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}
