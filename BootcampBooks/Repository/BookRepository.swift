//
//  BookRepository.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation

protocol BookRepository {
    func list(query: String) -> BookList
}

class BookRepositoryImpl: BookRepository {
    
    static let shared: BookRepository = BookRepositoryImpl()
    
    func list(query: String) -> BookList {
        
        var books: [Book] = []
        books.append(Book(title: "Promoting Sustainable Fisheries", description: "The International Legal and Policy Framework to Combat Illegal, Unreported and Unregulated Fishing", thumbnailPath: "http://books.google.com/books/content?id=ZTN8Fivl-AAC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api", pageCount: 341, publishedOn: "2010"))
        books.append(Book(title: "Fishing with Traps and Pots", description: "This manual describes the basic elements of fishing with traps and pots for small-scale fishermen. It presents the various types of traps and pots and their construction and gives guidance to choose the appropriate gear, how to rig it, how to use it to improve catch, how to select places to fish, soaking time and finally care of the catch.", thumbnailPath: "http://books.google.com/books/content?id=wdST3YhM1ZcC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api", pageCount: 62, publishedOn: "2001"))
        books.append(Book(title: "Fly-Fishing the Rocky Mountain Backcountry", description: "A detailed guide to fly-fishing destinations in the Rocky Mountains, many of which can only be reached by foot or on horseback, this book includes discussions on necessary gear AND strategies. The actual destinations are explored in depth. 33 color illustrations.", thumbnailPath: "http://books.google.com/books/content?id=8lj58VPvd50C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api", pageCount: 320, publishedOn: "1999-01"))
        books.append(Book(title: "Surf Fishing the Atlantic Coast", description: "Updated information on tackle, baits, and casting techniques and new photos and knot-tying illustrations Fishing sandbars, points, jetties, scalloped beaches, and inlets Species include bluefish, striped bass, red drum, weakfish, spotted sea trout, flounder, sharks", thumbnailPath: "http://books.google.com/books/content?id=LDnsP7XnLM0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api", pageCount: 182, publishedOn: "2005-09"))
        books.append(Book(title: "Fishing Boat Designs", description: "The first edition was published in 1977 and this edition completely revises and supersedes the Rev.1 edition (ISBN 9251040613)", thumbnailPath: "http://books.google.com/books/content?id=6yC3yfdQBtoC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api", pageCount: 64, publishedOn: "2004"))
        books.append(Book(title: "Ice Fishing", description: "Offers advice on clothing, tackle, and lures, tells how to catch perch, pike, walleyes, trout, and bluegills, and stresses the importance of safety precautions on the ice", thumbnailPath: "http://books.google.com/books/content?id=rUeFKvMSy-sC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api", pageCount: 164, publishedOn: "1985-11-01"))
        books.append(Book(title: "Economic Performance and Fishing Efficiency of Marine Capture Fisheries", description: "In order to safeguard the important role that marine capture fisheries play with regard to employment, income and food security, up-to-date information on the sector is needed to monitor the effect of management measures, regulations and government policies on its economic and financial health. This paper presents the findings of studies on the economic and financial performance of marine capture fisheries carried out in 13 countries during 2002 and 2003, and also contains the findings of two recent empirical studies on fishing efficiency presented at the 2004 session of the FAO/ICES Working Group on Fishing Technology and Fish Behaviour.", thumbnailPath: "http://books.google.com/books/content?id=RpIVQItOheQC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api", pageCount: 68, publishedOn: "2005"))
        books.append(Book(title: "Ecosystem effects of fishing", description: "", thumbnailPath: "", pageCount: 177, publishedOn: "2005"))
        let list = BookList()
        list.items = books
        return list
        
    }
}


