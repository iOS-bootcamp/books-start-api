//
//  ViewController.swift
//  BootcampBooks
//
//  Created by Keith Coughtrey on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import UIKit
import SVProgressHUD

class BookListController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var bookList: BookList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        bookList = BookRepositoryImpl.shared.list(query: "fishing")
    }

}

extension BookListController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let published = years()[section]
        return bookList?.items?.filter { published == $0.publishedYear }.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookCell", for: indexPath) as! BookCell
        let published = years()[indexPath.section]
        let books = bookList?.items?.filter { published == $0.publishedYear }
        if let book = books?[indexPath.row] {
            cell.configure(book: book)
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return years().count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let published = years()[section]
        return "Published: \(published)"
    }
    
    func years() -> [String] {
        if let publishedDates = bookList?.items?.map({$0.publishedYear ?? "Unknown"}) {
            return publishedDates.distinct.sorted()
        }
        return []
    }
}

extension BookListController : UITableViewDelegate {
    
}
